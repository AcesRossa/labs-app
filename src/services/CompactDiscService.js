import http from '../http-common';

export class CompactDiscService {
    getAll() {
        return http.get("/all")
    }

    create(data) {
        return http.post("/save", data)
    }

    findByArtist(artist) {
        return http.get(`/artist?artist=${artist}`)
    }
}

export default new CompactDiscService();