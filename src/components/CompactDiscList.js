import React, {Component} from 'react';
import CompactDiscService from "../services/CompactDiscService";

export default class CompactDiscList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            compactDiscs: []
        }
    }

    retrieveCompactDiscs() {
        CompactDiscService.getAll()
            .then(response => {
                this.setState({compactDiscs: response.data})
                console.log(response.data)
            })
            .catch(e => {
                console.log(e)
            })
    }

    componentDidMount() {
        this.retrieveCompactDiscs();
    }

    render() {
        const {compactDiscs} = this.state;
        return (
            <div className="col-md-6">
                <h4>Compact Discs List</h4>

                <ul className="list-group">
                    {compactDiscs &&
                    compactDiscs.map((compactDisc, index) => (
                        <li
                            className={"list-group-item "}
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            <b>Title:</b> {compactDisc.title} <b>Artist:</b> {compactDisc.artist} <b>Price:</b> {compactDisc.price}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}
