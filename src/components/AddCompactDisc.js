import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import CompactDiscService from "../services/CompactDiscService";

export default class AddCompactDisc extends Component {
    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeArtist = this.onChangeArtist.bind(this);
        this.onChangePrice = this.onChangePrice.bind(this);
        this.saveCompactDisc = this.saveCompactDisc.bind(this);

        this.state = {
            title: "",
            artist: "",
            price: 0
        }
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeArtist(e) {
        this.setState({
            artist: e.target.value
        });
    }

    onChangePrice(e) {
        this.setState({
            price: e.target.value
        });
    }

    saveCompactDisc() {
        let data = {
            title: this.state.title,
            artist: this.state.artist,
            price: this.state.price
        }


        CompactDiscService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if(this.state.submitted) {
            return <Redirect to="compactdiscs" />
        }
        return (
            <form>
                <h1>Add Compact Disc</h1>
                <div className="form-group">
                    <label htmlFor="exampleTitle">Title</label>
                    <input type="text" className="form-control" id="exampleTitle" aria-describedby="titleHelp"
                           placeholder="Enter title" value={this.state.title} onChange={this.onChangeTitle}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleArtist">Artist</label>
                    <input type="text" className="form-control" id="exampleArtist" aria-describedby="artistHelp"
                           placeholder="Enter artist" value={this.state.artist} onChange={this.onChangeArtist}/>
                </div>
                <div className="form-group">
                    <label htmlFor="examplePrice">Price</label>
                    <input type="number" className="form-control" id="examplePrice" aria-describedby="priceHelp"
                           placeholder="Enter price" value={this.state.price} onChange={this.onChangePrice}/>
                </div>
                <div className="form-group">
                    <input type="button" className="form-control" value="Save" onClick={this.saveCompactDisc}/>
                </div>
            </form>
        )
    }


}