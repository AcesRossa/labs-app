import React, {Component} from 'react';
import CompactDiscService from "../services/CompactDiscService";

export default class CompactDisc extends Component {
    constructor(props) {
        super(props);

        this.onChangeArtist = this.onChangeArtist.bind(this);
        this.searchCompactDiscByArtist = this.searchCompactDiscByArtist.bind(this)

        this.state = {
            artist: "",
            compactDiscs: []
        }
    }

    onChangeArtist(e) {
        this.setState({
            artist: e.target.value
        });
    }

    searchCompactDiscByArtist() {
        let data = {
            artist: this.state.artist
        }

        console.log("Data = ", data)

        CompactDiscService.findByArtist(data.artist)
            .then(response => {
                console.log("Response Data = ", response.data)
                this.setState({compactDiscs: response.data})
            })
            .catch(e => {
                console.log(e)
            })
    }

    renderCompactDisc() {
        let cdSize = Object.keys(this.state.compactDiscs).length
        const {compactDiscs} = this.state;
        if (cdSize !== 0) {
            return (
                <div className="col-md-6">
                    <ul className="list-group">
                        {compactDiscs &&
                        compactDiscs.map((compactDisc, index) => (
                            <li className={"list-group-item "} key={index}>
                                <b>Title:</b> {compactDisc.title} <b>Artist:</b> {compactDisc.artist} <b>Price:</b> {compactDisc.price}
                            </li>
                        ))}
                    </ul>
                </div>
            )
        }
    }

    render() {
        return (
            <>
                <form>
                    <h1>Find Compact Disc By Artist</h1>
                    <div className="form-group">
                        <label htmlFor="exampleArtist">Artist</label>
                        <input type="text" className="form-control" id="exampleArtist" aria-describedby="artistHelp"
                               placeholder="Enter Artist" value={this.state.artist} onChange={this.onChangeArtist}/>
                    </div>

                    <div className="form-group">
                        <input type="button" className="form-control" value="Search"
                               onClick={this.searchCompactDiscByArtist}/>
                    </div>
                </form>
                {this.renderCompactDisc()}
            </>
        )
    }

}