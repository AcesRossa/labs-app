import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// let age = 12;
// const customer = {
//     first_name: 'Bob',
//     last_name: 'Dylan'
// }
// const namesInHTML = [
//     <li key="bob">Bob Dust</li>,
//     <li key="freddy">Freddy Mercury</li>,
//     <li key="shazam">Shazam Nikola</li>,
//     <li key="wilibin">Wilibin Walabam</li>
// ]
//
// const content = <ul>{namesInHTML}</ul>
//
// const animals = ['Horse', 'Turtle', 'Elephant', 'Monkey']
//
// const animalsInHTML = animals.map((animal, i) => {
//     return <li key={i}>{animal}</li>
// })
//
// let main = (
//     <div>
//         <span>Hello there</span>
//         <span> Bob is { age } years old </span>
//         <h1>My name is {customer.first_name}</h1>
//         <h2>My last name is {customer.last_name}</h2>
//         {content}
//         <ul>{animalsInHTML}</ul>
//     </div>
// )
//
// const myDiv = document.getElementById('root')
//
// ReactDOM.render(main, myDiv);


