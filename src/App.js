import React from 'react';
import {Switch, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import CompactDiscList from "./components/CompactDiscList";
import CompactDisc from "./components/CompactDisc";
import AddCompactDisc from "./components/AddCompactDisc";

function App() {
    return (
        <div>
            <nav className="navbar navbar-expand navbar-dark bg-dark">
                <a href={"/compactdiscs"} className="navbar-brand">
                    bezKoder
                </a>
                <div className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link to={"/compactdiscs"} className="nav-link">
                            CompactDiscs
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/add"} className="nav-link">
                            Add
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/compactdiscs/findByArtist"} className="nav-link">
                            Find
                        </Link>
                    </li>
                </div>
            </nav>

            <div className="container mt-3">
                <Switch>
                    <Route exact path={["/", "/compactdiscs"]} component={CompactDiscList}/>
                    <Route exact path="/add" component={AddCompactDisc}/>
                    <Route path="/compactdiscs/findByArtist" component={CompactDisc}/>
                </Switch>
            </div>
        </div>
    );
}

export default App;
